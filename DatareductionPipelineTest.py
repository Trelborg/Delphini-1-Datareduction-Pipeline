# -*- coding: utf-8 -*-
'''
Datareduction pipeline 2.0

@author: Kristian Trelborg


Packages needed to run the program:
-----------------------------------
'''
import os
import pandas as pd
import numpy as np
import scipy.ndimage as nd
from scipy.optimize import curve_fit
from scipy.spatial.distance import cdist

#import matplotlib.pyplot as plt
#import os
#from collections import defaultdict
#from astropy.stats import biweight_location, sigma_clipped_stats, SigmaClip
#from photutils import CircularAperture, aperture_photometry, Background2D, MedianBackground
#from scipy.optimize import curve_fit

##########################################################################################################
#                                           csv file to array                                            #
##########################################################################################################
'''
Function to open csv file and return an array
--------------INPUT:
filename           : Input filename
    
-------------OUTPUT:
df.values          : Array containing the pixelvalues
'''

def CSV2Array (filename, rgb = False):
    pixelArray = np.asarray(pd.read_csv(filename))
    
    if rgb is True:
        g1 = pixelArray[::2].transpose()[::2].transpose()
        r = pixelArray[::2].transpose()[1::2].transpose()
        b = pixelArray[1::2].transpose()[::2].transpose()
        g2 = pixelArray[1::2].transpose()[1::2].transpose()
        return g1, r, b, g2
    else:
        return pixelArray

##########################################################################################################
#                                           RAW file to array                                            #
##########################################################################################################
'''
Function to open RAW file and return an array
--------------INPUT:
filename           : Input filename
width              : Width of the image (standard is set to full resolution of width = 2048 and height = 1536)
    
-------------OUTPUT:
pixelArray         : Array containing the pixelvalues

---------------NOTE:
Raw10Read and RawRead are auxiliary functions only used by Raw2Array
'''

def Raw10Read(raw10File):
    """ Read 5 bytes = 40 bits from a binary file and save as 4 10-bit decimal pixelvalues """
    while True:
        fiveBytes = raw10File.read(5)
        if len(fiveBytes) == 0:
            break
        # 'big' for big-endian. Raw10 is in Bin Endian, while raw is in Little Endian.
        fortyBits = int.from_bytes(fiveBytes, 'big') 

        #Split fortyBits into 4 10 bit integers
        pixelValues = []
        for i in range(4):
            pixelValues.append(fortyBits & 0b1111111111)
            fortyBits >>= 10
        yield from reversed(pixelValues)
        
def RawRead(rawFile):
    """ Read 4 bytes = 32 bits from a binary file and extract the 2 10-bit decimal pixelValues """
    # The 10 bit pixel value is padded with '11' on the left (MSB) and '0000' on the right (LSB) and the two pixels are swapped.
    while True:
        fourBytes = rawFile.read(4)
        if len(fourBytes) == 0:
            break
        # 'little' for little-endian.
        thirtytwoBits = int.from_bytes(fourBytes, 'little')
        # remove the 4 least significant bits and afterwards ignore the 2 most significant bits.
        pixelValues = []
        for i in range(2):
            thirtytwoBits >>= 4
            pixelValues.append(thirtytwoBits & 0b1111111111)
            thirtytwoBits >>= 12
        yield from reversed(pixelValues)

def Raw2Array(filename, width = 2048, rgb = False):
    with open(filename, 'rb') as raw:
        if filename.endswith("raw10"):
            pixelList = list(Raw10Read(raw))
        elif filename.endswith("raw"):
            pixelList = list(RawRead(raw))
        else:
            print("Error! Unknown filetype.")
    
    pixelArray = np.reshape(np.asarray(pixelList), (-1, width))
    
    if rgb is True:
        g1 = pixelArray[::2].transpose()[::2].transpose()
        r = pixelArray[::2].transpose()[1::2].transpose()
        b = pixelArray[1::2].transpose()[::2].transpose()
        g2 = pixelArray[1::2].transpose()[1::2].transpose()
        return g1, r, b, g2
    else:
        return pixelArray
#    return np.reshape(pixelArray, (-1, width))

##########################################################################################################
#                                             Check Distance                                             #
##########################################################################################################
'''
Function to check the distance between all the points in two arrays of coordinates
--------------INPUT:
array1             : Array with coordinates
array2             : Array with coordinates
    
-------------OUTPUT:
Output             : indices of the coordinates where two elements are closer together than threshold
'''

def CheckDistance (array1, array2, threshold, mask = False):
    dist = cdist(array1, array2)
    if mask == True:
        mdist = np.ma.masked_where(np.tril(dist) == 0, dist)
        indices = np.nonzero(mdist < threshold)
    elif mask == False: indices = np.nonzero(dist < threshold)
    # Check if the star is within the threshold of multiple stars and choose the closest one
    if len(np.transpose(indices)) > 1:
        print('There were ' + str(len(indices[0])) + ' stars at the following distances from the CoM: ' + str(dist[indices]))
        indices = np.nonzero(min(dist) == dist)
        print('The star at a distance of ' + str('{0:.1f}'.format(dist[indices][0])) + ' pixels was chosen.')
    return np.transpose(indices)

##########################################################################################################
#                                               STAR FINDER                                              #
##########################################################################################################
'''
Function to find the coordinates of the stars in the image
--------------INPUT:
pixelArray         : Input array with pixel values
sigma              : Number of standard deviations used as criteria to determine where the stars are
min_pix            : The minimum number of pixels (above the threshold) in a structure for it to count as a star
    
-------------OUTPUT:
CoM                : (y,x) coordinates of the Center of Mass
radius             : An estimate of the radius in pixels
'''

def StarFinder (pixelArray, sigma, min_pix, overlap = 20):       
    # define threshold as a number of standard deviations above the mean
    threshold = np.mean(pixelArray) + sigma*np.std(pixelArray)
    # find all pixels above the threshold
    above_threshold = np.where(pixelArray > threshold, 1, 0)
    # label the structures (where starmap = 1 that are adjacent to others)
    labels, N_stars = nd.label(above_threshold, structure = np.ones((3,3)))
    # sum the number of elements in each structure
    sums = nd.sum(above_threshold, labels, range(1,N_stars+1))
    # choose only structures with more than min_pix elements
    stars = np.where(sums > min_pix)
    # we need to add one because of index mismatch
    stars = stars[0] + 1    
    # define starmap as 0 where there are no stars and 1 where there are stars
    starmap = np.zeros(np.shape(pixelArray))
    for star in stars:
        starmap = starmap + np.where(labels == star, 1, 0)
    
    # label all the structures again
    labels, N_stars = nd.label(starmap, structure = np.ones((3,3)))
    # find the center of mass of all the stars found above
    CoM = nd.center_of_mass(pixelArray, labels, range(1,N_stars+1))

    # estimate the radius of the star in pixels
    radius = np.sqrt(sums[stars-1]/np.pi)
    # find and remove any overlapping stars (remove the smaller of the two)
    if len(CoM) > 1: # Only do so if there are more than one star
        indices = CheckDistance(CoM, CoM, overlap, mask = True)
        indices_to_remove = []
        for index in indices:
            if radius[index[0]] < radius[index[1]]:
                indices_to_remove.append(index[0])
            else:
                indices_to_remove.append(index[1])

        for index in sorted(indices_to_remove, reverse = True):
            del CoM[index]
            radius = np.delete(radius, index)
        N_stars = N_stars - len(indices_to_remove)
        
    CoM = np.asarray(CoM)    
    
    return CoM, radius, N_stars

##########################################################################################################
#                                             Signal/Noise                                               #
##########################################################################################################
'''
Function to calculate the Signal to Noise ratio (S/N)
--------------INPUT:
flux_star          : Flux from the star
flux_sky           : Flux from each sky pixel
N_pix              : Number of pixels used to find the star flux
    
-------------OUTPUT:
SNR                : Signal to Noise ratio
'''

def SNR (star_flux, sky_flux, N_pix):
    gain = 25 # electrons per LSB (or ADU)
    RON = 6 # Readout noise = 6 electrons RMS
#    return (gain*star_flux/np.sqrt(gain*star_flux + N_pix*gain*sky_flux + N_pix*RON**2))
    return (star_flux/np.sqrt(star_flux + sky_flux*N_pix + (RON/gain)**2 + (gain/2)**2*N_pix))

##########################################################################################################
#                                          Aperture size finder                                          #
##########################################################################################################
'''
Function to calculate the optimal aperture size to maximize S/N
--------------INPUT:
pixelArray         : Array with pixel values
CoM                : Center of Mass for a single star in (y,x) coordinates
R                  : Vector with [r, r_min, r_max]: radius of the aperture, inner and outer radius for the background
N                  : N is the number of iterations
    
-------------OUTPUT:
apertureSize       : The optimal aperture size
'''

def ApertureSizeFinder (pixelArray, CoM, R):
    # split the inputs
    r_min, r_max = int(R[1]), int(R[2]) # we don't need R[0]=r, but have left it there to make the functions similar
    # Remember that the CoM is defined as (y,x)
    x, y = int(CoM[1]), int(CoM[0])
    # define a grid going out to 2 pixels beyond the highest r value 
    x_grid, y_grid = np.meshgrid(range(max(x - r_max - 2, 0), min(x + r_max + 2, 2047)),range(max(y - r_max - 2, 0), min(y + r_max + 2, 1535)))
    # make a square frame of each image
    square = pixelArray[y_grid,x_grid]
    # define a circle
    C = np.sqrt((x_grid - x)**2 + (y_grid - y)**2)
    # calculate the background sky_flux
    sky_definition = (C >= r_min)*(C <= r_max)*square
    sky_mean = np.mean(sky_definition[np.nonzero(sky_definition)])
    sky_median = np.median(sky_definition[np.nonzero(sky_definition)])
    sky_flux = 3*sky_median - 2*sky_mean
#    sky_flux = sky_median
#    sky_flux = sky_mean
    
    SNR_r = np.zeros(int(r_max))
    radius = np.zeros(int(r_max))
    local_max_found = False
    for r in range(int(r_max)):
        # calculate the background corrected star_flux
        star_definition = (C <= r)*square
        star_intensities = star_definition[np.nonzero(star_definition)] - sky_flux
        star_flux = np.sum(star_intensities)
        # calculate the amount of pixels in the star (within r)
        N_pix = sum(sum(C <= r)) 
        SNR_r[r] = SNR(star_flux, sky_flux, N_pix)
        radius[r] = r
        
        if (r > 1 and local_max_found == False):
            if (SNR_r[r] <= SNR_r[r-1] and SNR_r[r-1] > SNR_r[r-2]): 
                local_max = [r-1, SNR_r[r-1]]
                local_max_found = True
            else: 
                local_max = [0, 0]
                local_max_found = False
    return SNR_r, radius, local_max       

##########################################################################################################
#                                           Aperture Photometry                                          #
##########################################################################################################
'''
Function to use aperture photometry to calculate the background corrected flux of the star
--------------INPUT:
pixelArray         : Array with pixel values
CoM                : Center of Mass for all the stars in (y,x) coordinates
R                  : Vector with [r, r_min, r_max]: radius of the aperture, inner and outer radius for the background
N                  : N is the number of iterations
    
-------------OUTPUT:
star_flux          : The flux of the star
sky_flux           : The estimated flux of each background pixel
N_pix              : The number of pixels within r (the size of the star)
'''

def AperturePhotometry (pixelArray, CoM, R):
    # split the inputs
    r, r_min, r_max = int(R[0]), int(R[1]), int(R[2])
    # Remember that the CoM is defined as (y,x)
    x, y = int(CoM[1]), int(CoM[0])
    # define a grid going out to 2 pixels beyond the highest r value
    x_grid, y_grid = np.meshgrid(range(max(x - r_max - 2, 0), min(x + r_max + 2, 2047)),range(max(y - r_max - 2, 0), min(y + r_max + 2, 1536)))
    # make a square frame around the star
    square = pixelArray[y_grid,x_grid]
    # define a circle
    C = np.sqrt((x_grid - x)**2 + (y_grid - y)**2)
    # calculate the background sky_flux
    sky_definition = (C >= r_min)*(C <= r_max)*square
    sky_mean = np.mean(sky_definition[np.nonzero(sky_definition)])
    sky_median = np.median(sky_definition[np.nonzero(sky_definition)])
    sky_flux = 3*sky_median - 2*sky_mean
#    sky_flux = sky_median
#    sky_flux = sky_mean
    # calculate the background corrected star_flux
    star_definition = (C <= r)*square
    star_intensities = star_definition[np.nonzero(star_definition)] - sky_flux
    star_max = max(star_intensities + sky_flux)
    star_flux = np.sum(star_intensities)
    # calculate the amount of pixels in the star (within r)
    N_pix = sum(sum(C <= r))
    
    return star_flux, sky_flux, N_pix, star_max

##########################################################################################################
#                                               User Fit                                                 #
##########################################################################################################

def UserFit (func, x, y):
    # popt is the fitted parameters
    popt, pcov = curve_fit(func, x, y)
    
    yFit = func(x, *popt)
    yMean = np.sum(y) / len(y)
    SStot = np.sum((y - yMean)**2)
    SSres = np.sum((yFit - y)**2)
    R2 = 1 - SSres / SStot
    
    # How to use the UserFit function:
    #def magFitFunc (x, p1, p2): return np.power(10,(p1*x + p2)/2.5)
    #(magFitParam, magFitR2) = UserFit(magFitFunc, All_Magnitudes, OverExposedPixelList)
    #plt.plot(Plot_Mag, magFitFunc(Plot_Mag, *magFitParam), 'b', label = 'UserFit curve: R^2 = ' + str('{0:.3f}'.format(magFitR2)))
    
    return popt, R2

''' Different functions to use '''
bias = 41.75 #3*np.median(MB) - 2*np.mean(MB) where MB is MasterBias
def linFitFunc_Bias (x, p1): return (x*p1 + bias)
def linFitFunc (x, p1, p2): return (x*p1 + p2)
def powerFunc (x, p1, p2): return (np.power(10, (x*p1 + p2)/2.5))
def backFunc (x): return (np.power(10, x/2.5))

##########################################################################################################
#                                           Expand Dataframe                                             #
##########################################################################################################
'''
Function to perform photometry on startrails
--------------INPUT:
pixelArray         : Array with pixel values
sigma              : Number of standard deviations used as criteria to determine where the stars are
min_pix            : The minimum number of pixels (above the threshold) in a structure for it to count as a star
    
-------------OUTPUT:
starFluxes         : The flux of all the stars in the image
starShapes         : The shape of the startrails

---------------NOTE:
It works well with sigma = 1 and min_pix = 50. You might be able to make sigma even smaller as long as min_pix is kept high.
'''
def ExpandDataframe (PhotometryDF, SaturationDF, SNRDF, refCoM, ExpTime, Magnitudes):      
    PhotometryDF['Magnitude'] = Magnitudes
    SaturationDF['Magnitude'] = Magnitudes
    SNRDF['Magnitude'] = Magnitudes
    
    # sort after magnitudes
    PhotometryDF = PhotometryDF.sort_values('Magnitude')
    SaturationDF = SaturationDF.sort_values('Magnitude')
    SNRDF = SNRDF.sort_values('Magnitude')
    
    # find the fit value at the highest integration time and put in a new column
    
    # for the Photometry:
    def linFitFunc (x, p1, p2): return (x*p1 + p2)
    MaskedDF = PhotometryDF.mask(SaturationDF > 1022) # removes saturated data and all nan 
    MaskedDF.drop(MaskedDF.columns[[-3,-2,-1]], axis = 1, inplace = True)       
    max_fit = []
    R2 = []
    for index, star in PhotometryDF.iterrows():
        # find the y values for the maskedDF and make a mask to remove all the nan values
        ym = MaskedDF.loc[index]
        mask_fit = np.isfinite(np.array(ym, dtype = float))
        if sum(mask_fit) > 2: # check that there are more than 2 points to make the fit    
            # fit the data and append to max_fit
            (linFitParam, linFitR2) = UserFit(linFitFunc, ExpTime[mask_fit], ym[mask_fit])
            max_fit.append(linFitFunc(ExpTime[-1], *linFitParam))
            R2.append(linFitR2)
        elif sum(mask_fit) <= 2:
            max_fit.append(np.nan)
            R2.append(np.nan)
    PhotometryDF['MaxFit'] = max_fit
    PhotometryDF['R2'] = R2
    
    # for the Saturation
    bias = 41.75 #3*np.median(MB) - 2*np.mean(MB) where MB is MasterBias
    def linFitFunc_Bias (x, p1): return (x*p1 + bias)
    MaskedDF = SaturationDF.mask(SaturationDF > 1022) # removes saturated data and all nan
    MaskedDF.drop(MaskedDF.columns[[-3,-2,-1]], axis = 1, inplace = True) 
    t_sat = []
    R2 = []
    for index, star in SaturationDF.iterrows():
        # find the y values for the maskedDF and make a mask to remove all the nan values
        ym = MaskedDF.loc[index]
        mask_fit = np.isfinite(np.array(ym, dtype = float))
        if sum(mask_fit) > 2: # check that there are more than 2 points to make the fit    
            # fit the data and append the saturation time (1023-bias)/p1 to t_sat, where p1 is linFit_BiasParam[0]
            (linFit_BiasParam, linFit_BiasR2) = UserFit(linFitFunc_Bias, ExpTime[mask_fit], ym[mask_fit])
            t_sat.append((1023-bias)/linFit_BiasParam[0])
            R2.append(linFit_BiasR2)
        elif sum(mask_fit) <= 2:
            t_sat.append(np.nan)
            R2.append(np.nan)
    SaturationDF['t_sat'] = t_sat
    SaturationDF['R2'] = R2
    
    return PhotometryDF, SaturationDF, SNRDF

##########################################################################################################
#                                    Complete Datareduction Pipeline                                     #
##########################################################################################################
'''
Function to perform photometry on startrails
--------------INPUT:
folder             : Folder where the images are stores (either raw or csv)
sigma              : Number of standard deviations used as criteria to determine where the stars are
min_pix            : The minimum number of pixels (above the threshold) in a structure for it to count as a star
R                  : Vector with [r, r_min, r_max]: radius of the aperture, inner and outer radius for the background
StarNames          : List with the names of all the stars
ExpTime            : List with all the exposure times
Magnitudes         : List with all the magnitudes
    
-------------OUTPUT:
PhotometryDF       : Dataframe with all the photometry fluxes for the different stars
SaturationDF       : Dataframe with all the saturation data for the different stars (highest single pixel value in eaach star)
refCoM             : The coordinates of all the stars found

---------------NOTE:
It works well with sigma = 2 and min_pix = 10.
'''

def DatareductionPipeline (folder, sigma, min_pix, R, StarNames = None, ExpTime = None, Magnitudes = None, overlap = 20, pickle = True, rgb = False):
    ''' Open the files in folder and save them as arrays for further analysis '''
    # define the path as the path of the 'folder' in the current folder
    path = os.path.join(os.path.dirname(__file__), folder)
    
    # Check if there are any pickled data to load
    if any(File.endswith(".pkl") for File in os.listdir(path)):
        print('Loading pickled DataFrames from ' + folder)
        PhotometryDF = pd.read_pickle(path+'/PhotometryDF.pkl')
        SaturationDF = pd.read_pickle(path+'/SaturationDF.pkl')
        SNRDF = pd.read_pickle(path+'/SNRDF.pkl')
        print('Loading saved refCoM from ' + folder)
        refCoM = np.load(path+'/refCoM.npy')
        # skip the rest of the DP and return the loaded values
        return PhotometryDF, SaturationDF, SNRDF, refCoM
    
    # columnNames is used afterwords to name the columns in Pandas DataFrame
    columnNames = []
    # refCoM is an array holding the reference values for the Center of Masses found. It is used to check the CoM's found against so we collect the same stars together
    refCoM = np.array([])
    Photometry = []
    Saturation = []
    SNRs = []
    # cycle through all the files in the folder and open them with the right function
    for counter, filename in enumerate(sorted(os.listdir(path))):
        (name, extension) = os.path.splitext(filename)
        print(name)
        if filename.endswith('.csv'):
            filename = os.path.join(path, filename)
            pixelArray = CSV2Array(filename)
        elif filename.endswith('.raw') or filename.endswith('.raw10'):
            filename = os.path.join(path, filename)
            pixelArray = Raw2Array(filename)
        
    
        
        ''' Find the stars and save their coordinates '''
        (CoM, radius, N_stars) = StarFinder(pixelArray, sigma, min_pix)    
        
             
        ''' Do Aperture Photometry to get corrected star fluxes '''
        if counter == 0: 
            refCoM = CoM
        StarFluxes = np.zeros(len(refCoM))
        StarMaxes = np.zeros(len(refCoM))
        StarSNRs = np.zeros(len(refCoM))
        for i, one_star in enumerate(CoM):
            (star_flux, sky_flux, N_pix, star_max) = AperturePhotometry(pixelArray, one_star, R)
            # Calculate the S/N
            star_SNR = SNR(star_flux, sky_flux, N_pix)
            # identify star
            cd = CheckDistance(refCoM, CoM[i:i+1], overlap)
            # the first value in cd is the corresponding star in refCoM. If len(cd) is 0 it is a new star
            if len(cd) == 0: # new star
                refCoM = np.append(refCoM, CoM[i:i+1], axis = 0)
                StarFluxes = np.append(StarFluxes, star_flux)
                StarMaxes = np.append(StarMaxes, star_max)
                StarSNRs = np.append(StarSNRs, star_SNR)
            elif len(cd) == 1: # old star
                # save the star_flux in the appropriate place defined by cd, where the first number is the star in refCoM the star is and the second number is just its position in its own CoM
                StarFluxes[cd[0][0]] = star_flux
                StarMaxes[cd[0][0]] = star_max
                StarSNRs[cd[0][0]] = star_SNR
                # Update refCoM
                refCoM[cd[0][0]] = one_star
            elif len(cd) > 1:
                print('the star is closer to ' + str(len(cd)) + ' stars')
        Photometry.append(StarFluxes)
        Saturation.append(StarMaxes)
        SNRs.append(StarSNRs)
        columnNames.append(name)
    
    ''' Define the DataFrame with all the data '''
    # Photometry data
    PhotometryDF = pd.DataFrame(Photometry)
    PhotometryDF = PhotometryDF.transpose()
    PhotometryDF = PhotometryDF.replace(0, np.nan)
    PhotometryDF.columns = columnNames
    # Saturation data
    SaturationDF = pd.DataFrame(Saturation)
    SaturationDF = SaturationDF.transpose()
    SaturationDF = SaturationDF.replace(0, np.nan)
    SaturationDF.columns = columnNames
    # Saturation data
    SNRDF = pd.DataFrame(SNRs)
    SNRDF = SNRDF.transpose()
    SNRDF = SNRDF.replace(0, np.nan)
    SNRDF.columns = columnNames
    
    x = [row[1] for row in refCoM]
    PhotometryDF['x'] = x
    SaturationDF['x'] = x
    SNRDF['x'] = x
    y = [row[0] for row in refCoM]
    PhotometryDF['y'] = y
    SaturationDF['y'] = y
    SNRDF['y'] = y

    
    if StarNames is not None:
        # Use the starnames as indices in the dataframes
        PhotometryDF.index = StarNames
        SaturationDF.index = StarNames
        SNRDF.index = StarNames
    
    if (ExpTime is not None and Magnitudes is not None):
        PhotometryDF, SaturationDF, SNRDF = ExpandDataframe(PhotometryDF, SaturationDF, SNRDF, refCoM, ExpTime, Magnitudes)
    
    if pickle is True:
        # DataFrames are saved as pkl-files (pickle)
        PhotometryDF.to_pickle(folder+'/PhotometryDF.pkl')
        SaturationDF.to_pickle(folder+'/SaturationDF.pkl')
        SNRDF.to_pickle(folder+'/SNRDF.pkl')
        # np arrays are save as npy-files
        np.save(folder+'/refCoM', refCoM)
    
    return PhotometryDF, SaturationDF, SNRDF, refCoM

##########################################################################################################
#                                          Startrail Photometry                                          #
##########################################################################################################
'''
Function to perform photometry on startrails
--------------INPUT:
pixelArray         : Array with pixel values
sigma              : Number of standard deviations used as criteria to determine where the stars are
min_pix            : The minimum number of pixels (above the threshold) in a structure for it to count as a star
    
-------------OUTPUT:
starFluxes         : The flux of all the stars in the image
starShapes         : The shape of the startrails

---------------NOTE:
It works well with sigma = 1 and min_pix = 50. You might be able to make sigma even smaller as long as min_pix is kept high.
'''

def StartrailPhotometry (pixelArray, sigma, min_pix):
    # define threshold as a number of standard deviations above the mean
    threshold = np.mean(pixelArray) + sigma*np.std(pixelArray)
    # find all pixels above the threshold
    above_threshold = np.where(pixelArray > threshold, 1, 0)
    # label the structures (where starmap = 1 that are adjacent to others)
    labels, N_stars = nd.label(above_threshold, structure = np.ones((3,3)))
    # sum the number of elements in each structure
    sums = nd.sum(above_threshold, labels, range(1,N_stars+1))
    # choose only structures with more than min_pix elements
    stars = np.where(sums > min_pix)
    # we need to add one because of index mismatch
    stars = stars[0] + 1    
    # define starmap as 0 where there are no stars and 1 where there are stars
    starmap = np.zeros(np.shape(pixelArray))
    for star in stars:
        starmap = starmap + np.where(labels == star, 1, 0)
    
    # label all the structures again
    labels, N_stars = nd.label(starmap, structure = np.ones((3,3)))
    # slice for each startrail encompassing the entire startrail
    slices = nd.find_objects(labels)
    # define arrays to hold the starfluxes and shapes for output
    starFluxes = np.zeros(N_stars)
    starShapes = []
    for index, one_slice in enumerate(slices):
        # the startrail slice with the pixelArray values
        pixelArray_slice = pixelArray[one_slice]
        # the same slice with the label values (0 for the background and a number for each structure starting at 1)
        label_slice = labels[one_slice]
        # define a background slice as the same slice, but the entire startrail has been masked (set to np.nan)
        background_slice = np.where(label_slice == 0, pixelArray_slice, np.nan)
        # calculate the background flux per pixel as 3*median - 2*mean (we use np.nanmedian and np.nanmean to ignore np.nan values)
        background_flux = 3*np.nanmedian(background_slice) - 2*np.nanmean(background_slice)
        # calculate the startrail flux as the sum of the pixelArray values of the star (star_slice) when they have been corrected for the background_flux
        starFluxes[index] = np.nansum(np.where(label_slice == 0, np.nan, pixelArray_slice) - background_flux)
        starShapes.append(np.shape(pixelArray_slice))
        
    return starFluxes, np.asarray(starShapes)

##########################################################################################################
#                                       Prepare to identify stars                                        #
##########################################################################################################
'''
Function to save a txt file with the x,y positions of the stars ordered after magnitude.
--------------INPUT:
Dataframe          : Dataframe with x and y positions with column names 'x' and 'y' sorted after magnitude or numpy array refCoM.
file_name          : Name of the output file
    
-------------OUTPUT:

---------------NOTE:
It saves a txt file with the positions that can be used on the Astrometry.net website to identify the stars.
It should also work with the CoM of startrails.
'''

def AstrometryNet (Input, file_name):
    precision = 5
    if isinstance(Input, pd.DataFrame):
        with open(file_name, 'w') as text_file:
            for row in Input.iterrows():
                print('%.*g\t%.*g' % (precision, row[1]['x'], precision, row[1]['y']), file = text_file)
    elif isinstance(Input, np.ndarray):
        with open(file_name, 'w') as text_file:
            for row in Input:
                print('%.*g\t%.*g' % (precision, row[1], precision, row[0]), file = text_file)

##########################################################################################################
#                                         Prepare for Period 04                                          #
##########################################################################################################
'''
Function to save a txt file with the x,y positions of the stars ordered after magnitude.
--------------INPUT:
time_in            : np.array with the times in days
signal_in          : np.array with the signal in magnitudes
file_name          : Name of the output file
    
-------------OUTPUT:

---------------NOTE:
It saves a txt file with the times and signals to use with Period04 to do discrete fourier transform
'''

def Period04 (time_in, signal_in, file_name):
    with open(file_name, 'w') as text_file:
        for (t_row, star_row) in zip(time_in, signal_in):
            print('%.*g\t%.*g' % (5, t_row, 5, star_row), file = text_file)

##########################################################################################################
#                                              Test Program                                              #
########################################################################################################## 

  
#pixelArray = CSV2Array('Alioth/Alioth_40s_Uncorrected.csv')
#
#(CoM, radius, N_stars) = StarFinder(pixelArray, 2, 10)
#
#fig, ax = plt.subplots()
#plt.imshow(pixelArray)
#for (star, r) in zip(CoM, radius):
#    y, x = star
#    C = plt.Circle((x,y), 15, color = 'lime', fill = False)
#    ax.add_patch(C)
#plt.show()
#
#R = [4, 15, 25]
#
#plt.figure()
#for star_CoM in CoM:
#    (SNR_r, local_max) = ApertureSizeFinder(pixelArray, star_CoM, R)
#    plt.plot(local_max[0], local_max[1], 'ko', markersize = 10)
##    plt.axvline(x = local_max[0], color = 'k')
#    plt.plot(range(R[2]), SNR_r, '-^')
#    plt.xlabel('Aperture Radius')
#    plt.ylabel('SNR')
#plt.show()
#
#StarPlot(pixelArray, CoM)
